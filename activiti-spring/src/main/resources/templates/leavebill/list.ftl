<html>
<#include "../common/header.ftl">

<body>
<div id="wrapper" class="toggled">

    <#--边栏sidebar-->
    <#include "../common/nav.ftl">

    <#--主要内容content-->
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-md-12 column">
                    <table class="table table-bordered table-condensed">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>申请人</th>
                            <th>事由</th>
                            <th>备注</th>
                            <th>状态</th>
                            <th>时间</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody>

                        <#list billList.content as billList>
                        <tr>
                            <td>${billList.id}</td>
                            <td>${billList.days}</td>
                            <td>${billList.content}</td>
                            <td>${billList.remark}</td>
                            <td><#if billList.status== 0>
                            	初始录入
                            	</#if>
                            	<#if billList.status== 1>
                            	审核中
                            	</#if>
                            	<#if billList.status== 2>
                            	审核完成
                            	</#if>
                            	<#if billList.status== 3>
                            	已删除
                            	</#if>
                            </td>
                            <td>${billList.leaveDate}</td>
                            <#if billList.status!= 0>
                            <td><a href="/activiti/workflow/deploye/viewHisComment?billId=${billList.id}">查看</a>
                            </#if>
                            <#if billList.status == 0>
                                    <a href="/activiti/workflow/bill/input?billId=${billList.id}">修改</a>
                                    <a href="/activiti/workflow/bill/del?id=${billList.id}">删除</a>
                                    <a href="/activiti/workflow/deploye/start?id=${billList.id}">开始申请</a>
                             </#if>
                            </td>
                        </tr>
                        </#list>
                        </tbody>
                    </table>
                </div>

            <#--分页-->
                <div class="col-md-12 column">
                    <ul class="pagination pull-right">
                    <#if currentPage lte 1>
                        <li class="disabled"><a href="#">上一页</a></li>
                    <#else>
                        <li><a href="/activiti/workflow/bill/list?userid=${Session.employee.id}&page=${currentPage - 1}&size=${size}">上一页</a></li>
                    </#if>

                    <#list 1..billList.getTotalPages() as index>
                        <#if currentPage == index>
                            <li class="disabled"><a href="#">${index}</a></li>
                        <#else>
                            <li><a href="/activiti/workflow/bill/list?userid=${Session.employee.id}&page=${index}&size=${size}">${index}</a></li>
                        </#if>
                    </#list>

                    <#if currentPage gte billList.getTotalPages()>
                        <li class="disabled"><a href="#">下一页</a></li>
                    <#else>
                        <li><a href="/activiti/workflow/bill/list?userid=${Session.employee.id}&page=${currentPage + 1}&size=${size}">下一页</a></li>
                    </#if>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>


</body>
</html>