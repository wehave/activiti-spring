<html>
<#include "../common/header.ftl">

<body>
<div id="wrapper" class="toggled">

<#--边栏sidebar-->
<#include "../common/nav.ftl">

<#--主要内容content-->
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-md-12 column">
                    <form role="form" method="post" action="/activiti/workflow/bill/create" >
                        <div class="form-group">
                            <label>时间</label>
                            <input name="days" type="text" class="form-control"  value="${(leaveBill.days)!''}"/>
                        </div>
                         <div class="form-group">
                            <label>内容</label>
                            <input name="content" type="area" class="form-control" value="${(leaveBill.content)!''}"/>
                        </div>
                        <div class="form-group">
                            <label>批注</label>
                            <input name="remark" type="text" class="form-control" value="${(leaveBill.remark)!''}"/>
                        </div>
                         <input name="id" type="hidden" class="form-control"  value="${(leaveBill.id)!''}"/>
                         <input name="status" type="hidden" class="form-control"  value="${(leaveBill.status)!''}"/>
                         <input name="userId" type="hidden" class="form-control"  value="${(leaveBill.userId)!''}"/>
                        <button type="submit" class="btn btn-default">提交</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
</body>
</html>