<html>
<#include "../common/header.ftl">

<body>
<div id="wrapper" class="toggled">

<#--边栏sidebar-->
<#include "../common/nav.ftl">

<#--主要内容content-->
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-md-12 column">
                    <form role="form" method="post" action="/activiti/workflow/deploye/upload"  enctype="multipart/form-data">
                        <div class="form-group">
                            <label>名称</label>
                            <input name="filename" type="text" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>文件</label>
                            <input name="file" type="file" class="form-control" />
                        </div>
                        <button type="submit" class="btn btn-default">提交</button>
                    </form>
                    <p class="bg-primary">部署信息管理列表</p>
                    <table class="table table-bordered table-condensed"  >
                       <thead>
                        <tr>
                            <th class="hidden-xs hidden-sm">流程ID</th>
                            <th class="col-xs-3">流程名称</th>
                            <th class="col-xs-3">发布时间</th>
                            <th class="col-xs-3">操作</th>
                        </tr>
                        </thead>
                      
                        <#list depList as depList>
                        <tr>
                            <td>${depList.id}</td>
                            <td>${depList.name}</td>
                            <td>${depList.deploymentTime?string('yyyy-MM-dd')}</td>
                            <td><a   class="btn btn-danger" href="/activiti/workflow/deploye/delete?deploymentId=${depList.id}">删除</a></td>
                       </tr>
                      </#list>
                      </table>
                      
                    <p class="bg-primary">流程定义信息列表</p>   
                    <table class="table table-bordered table-condensed"  >
                       <thead>
                        <tr>
                            <th >ID</th>
                            <th >名称</th>
                            <th >KEY</th>
                            <th >版本</th>
                            <th >规则文件</th>
                            <th >规则图片</th>
                            <th >部署ID</th>
                            <th >操作</th>
                        </tr>
                        </thead>
                      
                        <#list pdList as pdList>
                        <tr>
                            <td>${pdList.id}</td>
                            <td>${pdList.name}</td>
                            <td>${pdList.key}</td>
                            <td>${pdList.version}</td>
                            <td>${pdList.resourceName}</td>
                            <td>${pdList.diagramResourceName}</td>
                            <td>${pdList.deploymentId}</td>
                            <td><a   class="btn btn-primary" href="/activiti/workflow/deploye/vinit?deploymentId=${pdList.deploymentId}&imageName=${pdList.diagramResourceName}">查看</a></td>
                       </tr>
                      </#list>
                      </table>
                </div>
            </div>
        </div>
    </div>

</div>
</body>
</html>