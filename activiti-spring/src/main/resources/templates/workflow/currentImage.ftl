<html>
<#include "../common/header.ftl">

<body>
<div id="wrapper" class="toggled">

<#--边栏sidebar-->
<#include "../common/nav.ftl">

<#--主要内容content-->
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-md-12 column">
                <!-- 1.获取到规则流程图 -->
                <img src="/activiti/workflow/deploye/view?deploymentId=${deploymentId}&imageName=${imageName}">
           		 <!-- 2.根据当前活动的坐标，动态绘制DIV -->
				<div style="position: absolute;border:1px solid red;top:${acs.y}px;left: ${acs.x}px;width: ${acs.width}px;height:${acs.height}px;"></div>
            </div>       
        </div>
    </div>

</div>
</body>
</html>