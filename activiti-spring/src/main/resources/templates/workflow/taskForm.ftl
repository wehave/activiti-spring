<html>
<#include "../common/header.ftl">

<body>
<div id="wrapper" class="toggled">

    <#--边栏sidebar-->
    <#include "../common/nav.ftl">

    <#--主要内容content-->
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-md-12 column">
                 <p class="bg-primary">申请任务办理</p>
                  <form role="form" method="post" action="/activiti/workflow/deploye/submitTask" >
               			<div class="form-group">
                            <label>请假时间</label>
                            <input name="days" readonly type="text" class="form-control"  value="${leaveBill.days}"/>
                        </div>
                         <div class="form-group">
                            <label>请假原因</label>
                            <input name="content" readonly type="area" class="form-control" value="${leaveBill.content}"/>
                        </div>
                        <div class="form-group">
                            <label>备注</label>
                            <input name="remark"  readonly type="text" class="form-control" value="${leaveBill.remark}"/>
                        </div>
                        <div class="form-group">
                            <label>批注</label>
                            <input name="taskId"   class="form-control" value="${taskId}"/>
                            <input name="id"   class="form-control" value="${leaveBill.id}"/>
                            <textarea name="comment"   type="text" class="form-control" /></textarea>
                        </div>
                         <div class="form-group">
                        <#list outcomeList as outcomeList>
                        <input type="submit" name="outcome" value="${outcomeList}"/>
                        </#list>
                        </div>
                   </form>
                        
                      <p class="bg-primary">申请批注信息</p>
                      <table class="table table-bordered table-condensed">
                        <thead>
                        <tr>
                            <th>时间</th>
                            <th>批注人</th>
                            <th>批注信息</th>
                        </tr>
                        </thead>
                        <tbody>

                        <#list commentList as commentList>
                        <tr>
                      		<td>${commentList.time?string('yyyy-MM-dd')}</td>
                            <td>${commentList.userId}</td>
                            <td>${commentList.fullMessage}</td>
                        </tr>
                        </#list>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>


</body>
</html>