<html>
<#include "../common/header.ftl">

<body>
<div id="wrapper" class="toggled">

    <#--边栏sidebar-->
    <#include "../common/nav.ftl">

    <#--主要内容content-->
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-md-12 column">
                    <table class="table table-bordered table-condensed">
                        <thead>
                        <tr>
                            <th>任务ID</th>
                            <th>任务名称</th>
                            <th>创建时间</th>
                            <th>办理人</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody>

                        <#list tasks as tasks>
                        <tr>
                            <td>${tasks.id}</td>
                            <td>${tasks.name}</td>
                            <td>${tasks.createTime?string('yyyy-MM-dd')}</td>
                            <td>${tasks.assignee}</td>
                            <td>
                            <a href="/activiti/workflow/deploye/viewTaskForm?taskId=${tasks.id}">办理任务</a>
                            <a href="/activiti/workflow/deploye/viewCurrentImage?taskId=${tasks.id}">查看当前流程</a>
                            </td>
                        </tr>
                        </#list>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>


</body>
</html>