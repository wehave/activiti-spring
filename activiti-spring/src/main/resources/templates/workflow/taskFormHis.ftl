<html>
<#include "../common/header.ftl">

<body>
<div id="wrapper" class="toggled">

    <#--边栏sidebar-->
    <#include "../common/nav.ftl">

    <#--主要内容content-->
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-md-12 column">
                   <p class="bg-primary">申请的任务办理</p>
                      <table class="table table-bordered table-condensed">
                        <thead>
                        <tr>
                            <th>请假天数</th>
                            <th>请假原因</th>
                            <th>备注</th>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                      		<td>${leaveBill.days}</td>
                            <td>${leaveBill.content}</td>
                            <td>${leaveBill.remark}</td>
                        </tr>
                        </tbody>
                    </table>
                  
                        
                      <p class="bg-primary">申请批注信息</p>
                      <table class="table table-bordered table-condensed">
                        <thead>
                        <tr>
                            <th>时间</th>
                            <th>批注人</th>
                            <th>批注信息</th>
                        </tr>
                        </thead>
                        <tbody>

                        <#list commentList as commentList>
                        <tr>
                      		<td>${commentList.time?string('yyyy-MM-dd')}</td>
                            <td>${commentList.userId}</td>
                            <td>${commentList.fullMessage}</td>
                        </tr>
                        </#list>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="https://cdn.bootcss.com/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>


</body>
</html>