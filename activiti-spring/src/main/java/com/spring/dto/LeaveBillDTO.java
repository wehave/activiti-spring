package com.spring.dto;

import java.sql.Date;

import com.spring.enums.BillStatusEnum;
import com.spring.utils.EnumUtil;

import lombok.Data;

/**  
 * 创建时间：2018年6月25日 下午3:45:53  
 * 项目名称：activiti-spring  
 * @author Mo Xiaobao  
 * @version 1.0   
 * @since JDK 1.8  
 * 文件名称：LeaveBillDTO.java  
 * 类说明：  
 */
@Data
public class LeaveBillDTO {

	private Long id;
	
	private Integer days;
	
	private String content;
	
	private Date leaveDate;
	
	private String remark;
	
    private Integer userId;
	
	private Integer status;
	
	
	 public BillStatusEnum getBillStatusEnum() {
	        return EnumUtil.getByCode(status, BillStatusEnum.class);
	    }
}
