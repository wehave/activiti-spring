package com.spring.utils;


import org.springframework.web.context.request.RequestContextHolder;

import com.spring.dataobject.Employee;

public class SessionContext {

	public static final String GLOBLE_USER_SESSION = "globle_user";
	
	
	public static void setUser(Employee user){
		
		
		if(user!=null){
			RequestContextHolder.getRequestAttributes().setAttribute(GLOBLE_USER_SESSION, user,10000);
		}else{
			RequestContextHolder.getRequestAttributes().removeAttribute(GLOBLE_USER_SESSION,10000);
		}
	}
	
	public static Employee get(){
		Employee employee = (Employee)RequestContextHolder.getRequestAttributes().getAttribute(GLOBLE_USER_SESSION,10000);
		return  employee;
	}
}
