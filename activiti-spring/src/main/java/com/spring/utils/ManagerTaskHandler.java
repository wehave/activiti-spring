package com.spring.utils;


import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;

import com.spring.dataobject.Employee;

/**  
 * 创建时间：2018年6月27日 下午4:10:47  
 * 项目名称：activiti-spring  
 * @author Mo Xiaobao  
 * @version 1.0   
 * @since JDK 1.8  
 * 文件名称：ManagerTaskHandler.java  
 * 类说明：  
 */
public class ManagerTaskHandler implements TaskListener{

	/** serialVersionUID*/
	private static final long serialVersionUID = -8428958293205164522L;

	@Override
	public void notify(DelegateTask delegateTask) {
		
		Employee employee = SessionContext.get();
		//当前用户
		String name = employee.getParent().getName();
		delegateTask.setAssignee(name);
	}
	
}
