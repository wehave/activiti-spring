package com.spring.utils;

import java.io.File;
/**  
 * 创建时间：2018年6月22日 下午3:09:15  
 * 项目名称：activiti-spring  
 * @author Mo Xiaobao  
 * @version 1.0   
 * @since JDK 1.8  
 * 文件名称：FileUtils.java  
 * 类说明：  文件操作类
 */



public class FileUtils {

	public static void deleteFile(File... files) {
		for (File file : files) {
			if (file.exists()) {
				file.delete();
			}
		}
	}
}
