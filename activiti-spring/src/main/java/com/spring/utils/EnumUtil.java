package com.spring.utils;

import com.spring.enums.CodeEnum;

/**
 * 
 * Title: EnumUtil
 * Description:
 * Company:  
 * @author Mo Xiaobao
 * @date 2018年6月1日
 */
public class EnumUtil {

    public static <T extends CodeEnum> T getByCode(Integer code, Class<T> enumClass) {
        for (T each: enumClass.getEnumConstants()) {
            if (code.equals(each.getCode())) {
                return each;
            }
        }
        return null;
    }
}
