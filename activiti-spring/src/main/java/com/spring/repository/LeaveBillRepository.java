package com.spring.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.spring.dataobject.LeaveBill;

import java.lang.Integer;

/**  
 * 创建时间：2018年6月8日 下午2:53:58  
 * 项目名称：activiti-spring  
 * @author Mo Xiaobao  
 * @version 1.0   
 * @since JDK 1.8  
 * 文件名称：UserRepository.java  
 * 类说明：  
 */

public interface LeaveBillRepository extends JpaRepository<LeaveBill, Long>{

	 Page<LeaveBill> findByUserId(Integer userid,Pageable pageable);
	
}
