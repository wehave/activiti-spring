package com.spring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.spring.dataobject.Employee;

/**  
 * 创建时间：2018年6月8日 下午2:53:58  
 * 项目名称：activiti-spring  
 * @author Mo Xiaobao  
 * @version 1.0   
 * @since JDK 1.8  
 * 文件名称：UserRepository.java  
 * 类说明：  
 */

public interface EmployeeRepository extends JpaRepository<Employee, Long>{

	Employee findByName(String name);
	
}
