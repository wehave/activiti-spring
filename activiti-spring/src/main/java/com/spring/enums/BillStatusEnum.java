package com.spring.enums;

import lombok.Getter;

/**
 * 创建时间：2018年6月25日 下午3:47:04 项目名称：activiti-spring
 * 
 * @author Mo Xiaobao
 * @version 1.0
 * @since JDK 1.8 文件名称：BillStatusEnum.java 类说明：
 */
@Getter
public enum BillStatusEnum implements CodeEnum {
	NEW(0, "初始录入"), 
	GOING(1, "审核中"),
	FINISHED(2, "审核完成"),
	DELTET(3, "已删除");

	private Integer code;

	private String message;

	BillStatusEnum(Integer code, String message) {
		this.code = code;
		this.message = message;
	}
}
