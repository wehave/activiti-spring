package com.spring.enums;

/**
 * 
 * Title: CodeEnum
 * Description:
 * Company:  
 * @author Mo Xiaobao
 * @date 2018年6月1日
 */
public interface CodeEnum {
    Integer getCode();
}
