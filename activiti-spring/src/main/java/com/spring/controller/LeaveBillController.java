package com.spring.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.spring.dataobject.Employee;
import com.spring.dataobject.LeaveBill;
import com.spring.enums.BillStatusEnum;
import com.spring.service.LeaveBillService;

/**
 * 创建时间：2018年6月25日 下午2:52:24 项目名称：activiti-spring
 * 
 * @author Mo Xiaobao
 * @version 1.0
 * @since JDK 1.8 文件名称：LeaveBillController.java 类说明：
 */
@Controller
@RequestMapping("/workflow/bill")
public class LeaveBillController {

	@Autowired
	private LeaveBillService leaveBillService;

	/**
	 * 申请列表
	 * Title: home
	 * Description:
	 * @param @param userid
	 * @param @param page
	 * @param @param size
	 * @param @param map
	 * @param @return 
	 * @return ModelAndView
	 */
	@RequestMapping("/list")
	public ModelAndView home(Integer userid, @RequestParam(value = "page", defaultValue = "1") Integer page,
			@RequestParam(value = "size", defaultValue = "30") Integer size, Map<String, Object> map) {
		Pageable pageable = new PageRequest(page - 1, size);
		Page<LeaveBill> billList = leaveBillService.findBillByAll(userid, pageable);
		map.put("billList", billList);
		map.put("currentPage", page);
		map.put("size", size);
		return new ModelAndView("leavebill/list", map);
	}

	/**
	 * 添加申请
	 * Title: input
	 * Description:
	 * @param @param billId
	 * @param @param map
	 * @param @return 
	 * @return ModelAndView
	 */
	@RequestMapping("/input")
	public ModelAndView input(@RequestParam("billId")Long billId,
							  Map<String , Object> map){
		if (billId != 0) {
			LeaveBill leaveBill = leaveBillService.findLeaveBillById(billId);
			map.put("leaveBill", leaveBill);
			return new ModelAndView("leavebill/index",map);
		}
			return new ModelAndView("leavebill/index");
	}
	
	/**
	 * 保存/更新
	 * Title: create
	 * Description:
	 * @param @param leaveBill
	 * @param @return 
	 * @return ModelAndView
	 */
	@RequestMapping("/create")
	public ModelAndView create(LeaveBill leaveBill,HttpServletRequest request){
		
		HttpSession session = request.getSession();  
		Employee employee = (Employee) session.getAttribute("employee");
		leaveBill.setStatus(BillStatusEnum.NEW.getCode());
		leaveBill.setUserId(Integer.parseInt(employee.getId().toString()));
		leaveBillService.saveLeaveBill(leaveBill);
		return new ModelAndView("redirect:/workflow/bill/list?userid="+leaveBill.getUserId());
	}
	
	/**
	 * 删除
	 * Title: delete
	 * Description:
	 * @param @param id
	 * @param @return 
	 * @return ModelAndView
	 */
	@RequestMapping("del")
	public ModelAndView delete(@RequestParam("id") Long id){
		LeaveBill leaveBill = leaveBillService.findLeaveBillById(id);
		leaveBillService.deleteLeaveBillById(id);
		return new ModelAndView("redirect:/workflow/bill/list?userid="+leaveBill.getUserId());
	}
}
