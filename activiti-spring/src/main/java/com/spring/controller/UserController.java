package com.spring.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.spring.dataobject.Employee;
import com.spring.service.EmployeeService;
import com.spring.utils.SessionContext;

/**  
 * 创建时间：2018年6月25日 上午11:21:25  
 * 项目名称：activiti-spring  
 * @author Mo Xiaobao  
 * @version 1.0   
 * @since JDK 1.8  
 * 文件名称：UserController.java  
 * 类说明：  
 */

@Controller
@RequestMapping("/workflow/user")
public class UserController {
	
	
	@Autowired
	private EmployeeService employeeService;
	
	/**
	 * 初始化
	 * Title: login
	 * Description:
	 * @param @return 
	 * @return ModelAndView
	 */
	@RequestMapping("uinit")
	public ModelAndView login(){
		return new ModelAndView("/user/login");
	}

	/**
	 * 用户登陆
	 * Title: login
	 * Description:
	 * @param @return 
	 * @return ModelAndView
	 */
	@RequestMapping("login")
	public ModelAndView login(@RequestParam("name")String name,
							 HttpServletRequest request){
		Employee employee = employeeService.findEmployeeByName(name);
		HttpSession session = request.getSession();  
		session.setAttribute("employee", employee);
		SessionContext.setUser(employee);
		return new ModelAndView("/user/index");
	}
	
	/**
	 * 
	 * Title: login
	 * Description:
	 * @param @param request
	 * @param @return 
	 * @return ModelAndView
	 */
	@RequestMapping("logout")
	public ModelAndView login(HttpServletRequest request){
		HttpSession session = request.getSession();  
		session.setAttribute("employee", null);
		return new ModelAndView("/user/login");
	}
}
