package com.spring.form;

import java.io.File;

import lombok.Data;

/**  
 * 创建时间：2018年6月22日 上午11:22:57  
 * 项目名称：activiti-spring  
 * @author Mo Xiaobao  
 * @version 1.0   
 * @since JDK 1.8  
 * 文件名称：WorkflowForm.java  
 * 类说明：  
 */
@Data
public class WorkflowForm {

	private File file;		//流程定义部署文件
	private String filename;//流程定义名称
	
	private Long id;//申请单ID
	
	private String deploymentId;//部署对象ID
	private String imageName;	//资源文件名称
	private String taskId;		//任务ID
	private String outcome;		//连线名称
	private String comment;		//备注
}
