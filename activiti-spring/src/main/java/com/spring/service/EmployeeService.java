package com.spring.service;

import com.spring.dataobject.Employee;

/**  
 * 创建时间：2018年6月8日 下午2:56:45  
 * 项目名称：activiti-spring  
 * @author Mo Xiaobao  
 * @version 1.0   
 * @since JDK 1.8  
 * 文件名称：UserService.java  
 * 类说明：  
 */

public interface EmployeeService {

	Employee findEmployeeByName(String name);
	
}
