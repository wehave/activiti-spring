package com.spring.service;

import java.io.File;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.task.Comment;
import org.activiti.engine.task.Task;

import com.spring.dataobject.LeaveBill;
import com.spring.form.WorkflowForm;


/**  
 * 创建时间：2018年6月22日 上午11:35:06  
 * 项目名称：activiti-spring  
 * @author Mo Xiaobao  
 * @version 1.0   
 * @since JDK 1.8  
 * 文件名称：WorkflowService.java  
 * 类说明：  
 */
public interface WorkflowService {

	void saveNewDeploye(File file,String filename);

	List<Deployment> findDeploymentList();

	List<ProcessDefinition> findProcessDefinitionList();

	void deleteProcessDefinitionByDeploymentId(String deploymentId);

	InputStream findImageInputStream(String deploymentId, String imageName);

	void startProcess(Long id,String username);

	List<Task> findTaskListByName(String name);

	String findTaskFormKeyByTaskId(String taskId);

	LeaveBill findLeaveBillByTaskId(String taskId);

	List<Comment> findCommentByTaskId(String taskId);

	List<String> findOutComeListByTaskId(String taskId);

	void saveSubmitTask(WorkflowForm workflowForm,String username);

	ProcessDefinition findProcessDefinitionByTaskId(String taskId);

	Map<String, Object> findCoordingByTask(String taskId);

	List<Comment> findCommentByLeaveBillId(Long billId);
}
