package com.spring.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.spring.dataobject.LeaveBill;

/**  
 * 创建时间：2018年6月8日 下午2:56:45  
 * 项目名称：activiti-spring  
 * @author Mo Xiaobao  
 * @version 1.0   
 * @since JDK 1.8  
 * 文件名称：UserService.java  
 * 类说明：  
 */

public interface LeaveBillService {

	Page<LeaveBill> findBillByAll(Integer userid,Pageable pageable);

	LeaveBill findLeaveBillById(Long billId);

	void saveLeaveBill(LeaveBill leaveBill);

	void deleteLeaveBillById(Long id);

}
