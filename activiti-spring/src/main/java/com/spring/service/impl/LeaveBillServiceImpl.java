package com.spring.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.spring.dataobject.LeaveBill;
import com.spring.repository.LeaveBillRepository;
import com.spring.service.LeaveBillService;

/**  
 * 创建时间：2018年6月25日 下午2:51:42  
 * 项目名称：activiti-spring  
 * @author Mo Xiaobao  
 * @version 1.0   
 * @since JDK 1.8  
 * 文件名称：LeaveBillServiceImpl.java  
 * 类说明：  
 */
@Service
public class LeaveBillServiceImpl implements LeaveBillService{
	
	@Autowired
	private LeaveBillRepository repository;

	@Override
	public Page<LeaveBill> findBillByAll(Integer userid,Pageable pageable){
		
		return repository.findByUserId(userid,pageable);
	}

	@Override
	public LeaveBill findLeaveBillById(Long billId) {
		
		return repository.findOne(billId);
	}

	@Override
	public void saveLeaveBill(LeaveBill leaveBill) {
		repository.save(leaveBill);
	}

	@Override
	public void deleteLeaveBillById(Long id) {
		repository.delete(id);
	}

}
