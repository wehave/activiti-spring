package com.spring.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.dataobject.Employee;
import com.spring.repository.EmployeeRepository;
import com.spring.service.EmployeeService;

/**  
 * 创建时间：2018年6月22日 上午10:18:09  
 * 项目名称：activiti-spring  
 * @author Mo Xiaobao  
 * @version 1.0   
 * @since JDK 1.8  
 * 文件名称：EmployeeServiceImpl.java  
 * 类说明：  
 */
@Service
public class EmployeeServiceImpl implements EmployeeService{

	@Autowired
	private EmployeeRepository repository;
	
	@Override
	public Employee findEmployeeByName(String name) {
		Employee employee = repository.findByName(name);
		return employee;
	}

}
