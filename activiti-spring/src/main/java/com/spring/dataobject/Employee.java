package com.spring.dataobject;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

/**  
 * 创建时间：2018年6月8日 下午2:47:41  
 * 项目名称：activiti-spring  
 * @author Mo Xiaobao  
 * @version 1.0   
 * @since JDK 1.8  
 * 文件名称：User.java  
 * 类说明：  
 */
@Data
@Entity
public class Employee {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)  
	private Long id;
	
	private String name;
	
	private String password;
	
	private String role;
	
	private String email;
	
	 /**父组织*/
    @ManyToOne
    @JoinColumn(name="manager_id")
    private Employee parent;
    
    /**子组织*/
    @OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY)
    @JoinColumn(name="manager_id")
    private Set<Employee> children = new HashSet<Employee>();

}
