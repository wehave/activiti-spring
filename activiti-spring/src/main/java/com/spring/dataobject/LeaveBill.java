package com.spring.dataobject;



import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.DynamicUpdate;

import lombok.Data;

/**  
 * 创建时间：2018年6月22日 上午10:20:22  
 * 项目名称：activiti-spring  
 * @author Mo Xiaobao  
 * @version 1.0   
 * @since JDK 1.8  
 * 文件名称：LeaveBill.java  
 * 类说明：  
 */
@Data
@Entity
@DynamicUpdate
public class LeaveBill {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)  
	private Long id;
	
	private Integer days;
	
	private String content;
	
	private Date leaveDate = new Date();
	
	private String remark;
	
    private Integer userId;
	
	private Integer status = 0;
	
	

}
